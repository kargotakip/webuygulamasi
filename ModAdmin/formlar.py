from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired


class Adminformu(FlaskForm):
    mail = StringField(label='Email Adresiniz', validators=[DataRequired(message='boş geçilemez.')])
    sifre = PasswordField(label='Şifreniz', validators=[DataRequired(message='Boş Geçilemez')])
    gonder = SubmitField(label='Giriş')
