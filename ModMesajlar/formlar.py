from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField
from wtforms.validators import DataRequired


class mesajformu(FlaskForm):

    ad = StringField(label='Adınızı giriniz', validators=[DataRequired(message='boş geçilemez.')])
    soyad = StringField(label='Soyadınızı Giriniz', validators=[DataRequired(message='boş geçilemez.')])
    mail = StringField(label='e-Mail Giriniz', validators=[DataRequired(message='boş geçilemez.')])
    mesaj = TextAreaField(label='Mesajı Giriniz', validators=[DataRequired(message='boş geçilemez.')])
    gonder = SubmitField(label='Kaydet')

class mesajduzenleformu(FlaskForm):
    ad = StringField(label='Adınızı giriniz', validators=[DataRequired(message='boş geçilemez.')])
    soyad = StringField(label='Soyadınızı Giriniz', validators=[DataRequired(message='boş geçilemez.')])
    mail = StringField(label='e-Mail Giriniz', validators=[DataRequired(message='boş geçilemez.')])
    mesaj = TextAreaField(label='Mesajı Giriniz', validators=[DataRequired(message='boş geçilemez.')])
    mesaj2 = TextAreaField(label='Mesajı Giriniz', validators=[DataRequired(message='boş geçilemez.')])
    gonder = SubmitField(label='Kaydet')
