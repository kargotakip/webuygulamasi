from flask import Blueprint, render_template, redirect, url_for, request, abort, jsonify
from .formlar import *
from .model import *

mod_mesajlar = Blueprint('anasayfa', __name__)


@mod_mesajlar.route('/anasayfa')
@mod_mesajlar.route('/')
def Kargotakip():
    mesaj = mesajlar.query.all()
    return render_template('Kargotakipanasayfa.html', veri=mesaj)


@mod_mesajlar.route('/fiyat')
def fiyat():
    mesaj = mesajlar.query.all()

    return render_template('Kargotakipfiyatalma.html', veri=mesaj)


@mod_mesajlar.route('/kargo')
def kargo():
    mesaj = mesajlar.query.all()

    return render_template('Kargotakipkargoverme.html', veri=mesaj)


@mod_mesajlar.route('/sozlesme')
def sozlesme():
    mesaj = mesajlar.query.all()

    return render_template('Kargotakipsözlesme.html', veri=mesaj)


@mod_mesajlar.route('/hedef')
def hedef():
    mesaj = mesajlar.query.all()

    return render_template('Kargotakiphedefimiz.html', veri=mesaj)


@mod_mesajlar.route('/iletisim', methods=['POST', 'GET'])
def iletisim():
    mesaj = mesajlar.query.all()
    mesajform = mesajformu()
    if mesajform.validate_on_submit():
        yenimesaj = mesajlar()
        yenimesaj.ad = mesajform.ad.data
        yenimesaj.soyad = mesajform.soyad.data
        yenimesaj.mail = mesajform.mail.data
        yenimesaj.mesaj = mesajform.mesaj.data
        db.session.add(yenimesaj)
        db.session.commit()
        return redirect(url_for('anasayfa.Kargotakip'))
        # form doldurmuş kaydedelim
    return render_template('Kargotakipiletisim.html', veri=mesaj,
                           form=mesajform)  # boş formu gösterelim


@mod_mesajlar.route("/sil", methods=['POST'])
def mesajSil():
    mesajid = request.form["id"]

    silinecekmesaj = mesajlar.query.filter(mesajlar.id == mesajid).one_or_none()
    if silinecekmesaj is None:
        abort(404)
    db.session.delete(silinecekmesaj)
    db.session.commit()

    return jsonify({'sonuc': 'TAMAM'})


@mod_mesajlar.route('/liste', methods=['POST', 'GET'])
def liste():
    mesaj = mesajlar.query.all()
    return render_template('mesajlar.html', veri=mesaj,hata="")


@mod_mesajlar.route('/düzenle/<int:id>', methods=['POST', 'GET'])
def duzenle(id):
    duzenlenecekders = mesajlar.query.filter(mesajlar.id == id).one_or_none()
    duzenleform = mesajduzenleformu()
    if duzenleform.validate_on_submit():
        duzenlenecekders.yoneticimesaj = duzenleform.mesaj2.data
        duzenlenecekders.ad=duzenleform.ad.data
        duzenlenecekders.soyad = duzenleform.soyad.data
        duzenlenecekders.mail = duzenleform.mail.data
        duzenlenecekders.mesaj = duzenleform.mesaj.data
        db.session.commit()
        return redirect(url_for('anasayfa.liste'))

    # ilk kez form gösterilecekse yada hata varsa.
    duzenleform.mesaj2.data = duzenlenecekders.yoneticimesaj
    duzenleform.mesaj.data=duzenlenecekders.mesaj
    duzenleform.ad.data=duzenlenecekders.ad
    duzenleform.soyad.data=duzenlenecekders.soyad
    duzenleform.mail.data=duzenlenecekders.mail

    return render_template('düzenle.html', form=duzenleform,hata="")
