from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_wtf.csrf import CSRFProtect
from flask_login import LoginManager
app = Flask(__name__)

app.config['SECRET_KEY'] ='ÇOK GİZLİ'
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///kargo.tkp'
csrf=CSRFProtect(app)
db=SQLAlchemy(app)
login=LoginManager(app)

from ModKullanıcı.yonetici import mod_kullanici
from ModMesajlar.yonetici import mod_mesajlar
from ModAdmin.yonetici import mod_admin

migrate=Migrate(app,db)


app.register_blueprint(mod_kullanici,url_prefix='/kullanici')
app.register_blueprint(mod_mesajlar,url_prefix='/anasayfa')
app.register_blueprint(mod_admin,url_prefix='/admin')
app.register_blueprint(mod_admin,url_prefix='/')

if __name__ == '__main__':
    app.run()