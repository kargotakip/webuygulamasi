from app import db
from ModMesajlar.model import mesajlar


class Kullanici(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mail = db.Column(db.String(50))
    sifre = db.Column(db.String(50))
    adres = db.Column(db.String(250))
    telefon = db.Column(db.Integer)

